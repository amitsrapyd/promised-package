export declare function mapSeries(iter: any[], mupper: (val: any, i?: number, len?: number) => Promise<any>): Promise<any[]>;

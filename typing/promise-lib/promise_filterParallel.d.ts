export declare function filterParallel(iter: any[], func: (val: any, i?: number, len?: number) => Promise<boolean>): Promise<any[]>;

interface IPromiseObj {
    [k: string | number]: any;
}
export async function props(obj: IPromiseObj) {
    const newObj: IPromiseObj = {};
    for (const key in obj) {
        const res = await obj[key];
        newObj[key] = res;
    }
    return newObj;
}
// const promise1 = Promise.resolve(3);
// const promise2 = 42;
// const promise3 = new Promise((resolve, reject) => {
//     setTimeout(resolve, 100, 'foo');
// });
// props({ promise1, promise2, promise3 })
//     .then((resObj) => {
//         console.log("finished props!");
//         console.log("object: ", resObj);
//     })

export async function filterSeries(
    iter: any[],
    func: (val: any, i?: number, len?: number) => Promise<boolean>
) {
    const result = [];
    for (let i = 0; i < iter.length; i++) {
        const value = await iter[i];
        if (await func(value, i, iter.length)) {
            result.push(value);
        }
    }
    return result;
}

// const promiseArr = [Promise.resolve(3), 42, new Promise((resolve) => {
//     setTimeout(resolve, 100, 'foo');
// })];

// console.log(await filterSeries(promiseArr, async (value) => {
//     return typeof value === 'number';
// }));

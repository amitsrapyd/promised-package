export async function all(arr: (Promise<any> | any)[]) {
    // if (!Array.isArray(arr)) {
    //     throw new Error("function 'all' get array as param");
    // }
    const newArr = [];
    for (const p of arr) {
        const res = await p;
        newArr.push(res);
    }
    return newArr;
}

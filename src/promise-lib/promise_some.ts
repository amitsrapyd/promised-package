export function some(iter: any[], count: number) {
    return new Promise((resolve) => {
        let counter = 0;
        const arr: any[] = [];
        for (const item of iter) {
            if (item instanceof Promise) {
                item.then((res) => {
                    //res is the result of the promise when he finished
                    if (counter < count) {
                        counter++;
                        arr.push(res);
                    } else {
                        resolve(arr);
                    }
                });
            } else if (counter < count) {
                counter++;
                arr.push(item);
            } else {
                resolve(arr);
            }
        }
    });
}

// console.log(await some([
//     Promise.resolve("ns1.example.com"),
//     Promise.resolve("ns2.example.com"),
//     Promise.resolve("ns3.example.com"),
//     "ns4.example.com"
// ], 2));

import { expect } from "chai";
import { race } from "../src/promise-lib/promise_race";

describe("Promise race module", () => {
    context("#race", () => {
        it("should exist", () => {
            expect(race).to.be.a("function");
            expect(race).to.be.instanceOf(Function);
        });
        it("should return val that first resolved", async () => {
            const promise1 = new Promise((resolve) => {
                setTimeout(resolve, 200, 3);
            });
            const promise2 = new Promise((resolve) => {
                setTimeout(resolve, 300, 42);
            });
            const promise3 = new Promise((resolve) => {
                setTimeout(resolve, 100, "foo");
            });
            const promise4 = new Promise((resolve) => {
                setTimeout(resolve, 400, true);
            });
            const val = await race([promise1, promise2, promise3, promise4]);
            expect(val).to.eql("foo");
        });
    });
});
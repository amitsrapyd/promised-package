import { expect } from "chai";
import { each } from "../src/promise-lib/promise_each";

describe("Promise each module", () => {
    context("#each", () => {
        it("should exist", () => {
            expect(each).to.be.a("function");
            expect(each).to.be.instanceOf(Function);
        });
        it("should return array after change", async () => {
            const promiseArr: (Promise<any> | any)[] = [Promise.resolve(3), 42, new Promise((resolve) => {
                setTimeout(resolve, 500, "foo");
            })];
            const valArr: any[] = [];
            await each(promiseArr, async (value) => { valArr.push((await value) + 1); });
            expect(valArr).to.eql([4, 43, "foo1"]);
        });
    });
});